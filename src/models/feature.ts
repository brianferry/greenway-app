export default class Feature{
    attributes: Array<Attribute>;
}

class Attribute{
    TRAIL_NAME: string;
    OWNER: string;
    TRAIL_STATUS: string;
    LENGTH: number;
    DISTANCE: number;
}
