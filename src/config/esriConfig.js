const esriSettings = {
    wkid: 102100,
    measurement: "esriSRUnit_Meter",
    distanceExtentUnit: 10000,
    distanceSearchUnit: "miles",
    geoServiceURL: "http://sampleserver6.arcgisonline.com/arcgis/rest/services/Utilities/Geometry/GeometryServer"
}

export default esriSettings;