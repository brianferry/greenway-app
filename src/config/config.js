const config = [
    {
        ServiceName: "Greenway Trails",
        ServiceURL: "https://maps.wakegov.com/arcgis/rest/services/OpenSpace/Greenways/MapServer/0",
        ServiceNameField: "TRAIL_NAME",
        LayerType: "Map"
    }
    ,{
        ServiceName: "Public Libraries",
        ServiceURL: "https://services1.arcgis.com/a7CWfuGP5ZnLYE7I/arcgis/rest/services/Libraries/FeatureServer/0",
        ServiceNameField: "NAME",
        LayerType: "Feature"
    }
];

export default config;