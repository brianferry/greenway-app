import { NgModule } from '@angular/core';
import { GreenwayMapComponent } from './greenway-map/greenway-map';
import { IonicModule } from 'ionic-angular';
@NgModule({
	declarations: [GreenwayMapComponent],
	imports: [IonicModule.forRoot(GreenwayMapComponent),],
	exports: [GreenwayMapComponent]
})
export class ComponentsModule {}
