import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Platform } from 'ionic-angular';
import { loadModules } from 'esri-loader';
import config from '../../config/config';
import esriSettings from '../../config/esriConfig';

@Component({
  selector: 'greenway-map',
  templateUrl: 'greenway-map.html'
})
export class GreenwayMapComponent implements OnInit {

  @ViewChild('map') mapEl: ElementRef;
  @ViewChild('address') addressElement: ElementRef;

  //Application Setting - title Bar
  title: string = "Wake County Search";
  
  //Globals for function calls
  mapViewZoom: any;
  switchActiveLayer: any;
  getResults: any;

  //UI variables, defaults features to empty array, shows loading icon, and shows results if queried
  features: Array<any>;
  loading: false;
  queried: false;

  //Dropdown selector, defaulted to index 0.
  Selected: string = "0";

  //Field switcher for the results field, defaulted to the one at index 0. 
  ServiceNameField: string = config[0].ServiceNameField;

  //Selected URL switcher for the distance function.
  SelectedURL: string = config[0].ServiceURL;

  //Sets up the maps default constructor.  
  async getGeo() {
    await this._platform.ready();

    const [
            Map
            , MapView
            , Search
            , DistanceParameters
            , GeometryService
            , FeatureLayer
            , Extent
            , SpatialReference
            , Polyline
            , Point
          ]: any
      = await loadModules([
        'esri/Map',
        'esri/views/MapView',
        'esri/widgets/Search',
        'esri/tasks/support/DistanceParameters',
        'esri/tasks/GeometryService',
        'esri/layers/FeatureLayer',
        'esri/geometry/Extent',
        'esri/geometry/SpatialReference',
        'esri/geometry/Polyline',
        'esri/geometry/Point'
      ]);

    let map = new Map({
      basemap: 'topo'
    });

    this.mapViewZoom = function (evt) {

      let polygon = {};
      if (evt.geometry.paths !== undefined) {
        polygon = new Polyline({
          type: "polyline",
          hasZ: false,
          hasM: true,
          paths: evt.geometry.paths,
          spatialReference: { wkid: esriSettings.wkid }
        });
      }
      else {
        polygon = new Point({
          x: evt.geometry.x,
          y: evt.geometry.y,
          spatialReference: { wkid: esriSettings.wkid }
        })
      }

      mapView.goTo({ target: polygon, zoom: 15 });
    }

    this.switchActiveLayer = function (evt) {
      this.ServiceNameField = config[evt].ServiceNameField
      this.SelectedURL = config[evt].ServiceURL;

      map.layers.forEach(element => {
        element.visible = false;
      });

      let evtNum = parseInt(evt);

      map.layers.items[evtNum].visible = true;

      this.features = [];
    }

    this.getResults = function (self: any, evt: any) {
      this.loading = true;
      this.queried = true;
      this.features = Array<any>();
      let x = evt.result.feature.geometry.x;
      let y = evt.result.feature.geometry.y;

      this._http.get(this.SelectedURL + `/query?outFields=*&geometry=${x}%2C${y}&geometryType=esriGeometryPoint&inSR=${esriSettings.wkid}&returnGeometry=true&returnCurves=false&spatialRel=esriSpatialRelIntersects&distance=${esriSettings.distanceExtentUnit}&units=${esriSettings.measurement}&outSR=${esriSettings.wkid}&f=pjson`)
        .subscribe((data: any) => {
          if (data !== undefined && Object.keys(data).length !== 0 && data.features !== undefined) {
            let _features = data.features.map((response: Array<any>) => response);
            _features.forEach((element: any) => {
              if (this.features.findIndex(a => a.attributes.TRAIL_NAME === element.attributes.TRAIL_NAME) <= -1) {
                this.features.push(element);
              }
            });

            let geoService = new GeometryService(esriSettings.geoServiceURL);
            this.geoDistance(geoService, DistanceParameters, evt);
          }
        });
    }

    config.forEach(layer => {
      map.layers.add(new FeatureLayer({
        url: layer.ServiceURL,
        visible: false
      }));
    });

    map.layers.items[0].visible = true;

    let ext = new Extent({
      spatialReference: new SpatialReference({ wkid: esriSettings.wkid })
    })

    // Inflate and display the map
    let mapView = new MapView({
      // create the map view at the DOM element in this component
      container: this.mapEl.nativeElement,
      center: [-78.6382, 35.7796],
      zoom: 12,
      map: map,
      extent: ext
    });

    var search = new Search({
      view: mapView,
      container: this.addressElement.nativeElement
    });

    search.on("select-result", ((evt) => this.getResults(this, evt)));
  }

  constructor(public _platform: Platform, public _http: HttpClient) {
  }

  //Takes the (global) list of features, DistanceParameter object, and evt from the main function and calculates the distance between the inputted point and the geometry using the ESRI default Geometry Service 
  geoDistance = function (geometryService: any, DistanceParameters: any, evt: any){
    this.features.forEach(element => {
      let distParam = new DistanceParameters();
      distParam.geometry1 = evt.result.feature.geometry;
      distParam.geometry2 = element.geometry;
      distParam.geodesic = true;
      distParam.distanceUnit = esriSettings.distanceSearchUnit;

      
      geometryService.distance(distParam).then(function (calcDistance) {
        element.distance = calcDistance.toPrecision(2);
      });
    });

    this.loading = false;
  }

  //Sorts the list by the 'Name' field in the list. 
  sortBy(prop) {
    return this.features.sort((a, b) => a[prop] > b[prop] ? 1 : a[prop] === b[prop] ? 0 : -1);
  }

  //Initialize the map
  ngOnInit() {
    this.getGeo();
  }
}