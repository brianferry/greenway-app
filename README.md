# Greenway App using the Wake County Open Data GIS Services

Example application that was created to learn Ionic for mobile application development.

Application takes in an address entered into the search bar, maps it with the default ESRI search function, and runs the data through a geometry service to get the distance.

### **Services used are:**

[Wake County Greenway Map Service](https://maps.wakegov.com/arcgis/rest/services/OpenSpace/Greenways/MapServer/0)

[Wake County Library Feature Service](https://services1.arcgis.com/a7CWfuGP5ZnLYE7I/arcgis/rest/services/Libraries/FeatureServer/0)

[ESRI Geometry Service](http://sampleserver6.arcgisonline.com/arcgis/rest/services/Utilities/Geometry/GeometryServer)



### **Two configuration files are used:**

**config.js** (Layer Configuration for map services)

**esriConfig.js** (ESRI Configuration for Arcgis JavaScript functions)




To begin, run **npm install** first and then **ionic serve** to build the project.
