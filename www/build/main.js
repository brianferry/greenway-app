webpackJsonp([0],{

/***/ 110:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 110;

/***/ }),

/***/ 152:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 152;

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
        this.title = "Wake County Search";
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\brian.ferry\Downloads\New folder\greenway-app\src\pages\home\home.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>\n\n      {{ title }}\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <greenway-map></greenway-map>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\brian.ferry\Downloads\New folder\greenway-app\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(219);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_components_module__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_common_http__ = __webpack_require__(197);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_7__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_common_http__["b" /* HttpClientModule */]
            ],
            exports: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_1__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Users\brian.ferry\Downloads\New folder\greenway-app\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\brian.ferry\Downloads\New folder\greenway-app\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__greenway_map_greenway_map__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__greenway_map_greenway_map__["a" /* GreenwayMapComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_1__greenway_map_greenway_map__["a" /* GreenwayMapComponent */]),],
            exports: [__WEBPACK_IMPORTED_MODULE_1__greenway_map_greenway_map__["a" /* GreenwayMapComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GreenwayMapComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_esri_loader__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_esri_loader___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_esri_loader__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_config__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config_esriConfig__ = __webpack_require__(278);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var GreenwayMapComponent = /** @class */ (function () {
    function GreenwayMapComponent(_platform, _http) {
        this._platform = _platform;
        this._http = _http;
        //Application Setting - title Bar
        this.title = "Wake County Search";
        //Dropdown selector, defaulted to index 0.
        this.Selected = "0";
        //Field switcher for the results field, defaulted to the one at index 0. 
        this.ServiceNameField = __WEBPACK_IMPORTED_MODULE_4__config_config__["a" /* default */][0].ServiceNameField;
        //Selected URL switcher for the distance function.
        this.SelectedURL = __WEBPACK_IMPORTED_MODULE_4__config_config__["a" /* default */][0].ServiceURL;
        //Takes the (global) list of features, DistanceParameter object, and evt from the main function and calculates the distance between the inputted point and the geometry using the ESRI default Geometry Service 
        this.geoDistance = function (geometryService, DistanceParameters, evt) {
            this.features.forEach(function (element) {
                var distParam = new DistanceParameters();
                distParam.geometry1 = evt.result.feature.geometry;
                distParam.geometry2 = element.geometry;
                distParam.geodesic = true;
                distParam.distanceUnit = __WEBPACK_IMPORTED_MODULE_5__config_esriConfig__["a" /* default */].distanceSearchUnit;
                geometryService.distance(distParam).then(function (calcDistance) {
                    element.distance = calcDistance.toPrecision(2);
                });
            });
            this.loading = false;
        };
    }
    //Sets up the maps default constructor.  
    GreenwayMapComponent.prototype.getGeo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var _a, Map, MapView, Search, DistanceParameters, GeometryService, FeatureLayer, Extent, SpatialReference, Polyline, Point, map, ext, mapView, search;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this._platform.ready()];
                    case 1:
                        _b.sent();
                        return [4 /*yield*/, Object(__WEBPACK_IMPORTED_MODULE_3_esri_loader__["loadModules"])([
                                'esri/Map',
                                'esri/views/MapView',
                                'esri/widgets/Search',
                                'esri/tasks/support/DistanceParameters',
                                'esri/tasks/GeometryService',
                                'esri/layers/FeatureLayer',
                                'esri/geometry/Extent',
                                'esri/geometry/SpatialReference',
                                'esri/geometry/Polyline',
                                'esri/geometry/Point'
                            ])];
                    case 2:
                        _a = _b.sent(), Map = _a[0], MapView = _a[1], Search = _a[2], DistanceParameters = _a[3], GeometryService = _a[4], FeatureLayer = _a[5], Extent = _a[6], SpatialReference = _a[7], Polyline = _a[8], Point = _a[9];
                        map = new Map({
                            basemap: 'topo'
                        });
                        this.mapViewZoom = function (evt) {
                            var polygon = {};
                            if (evt.geometry.paths !== undefined) {
                                polygon = new Polyline({
                                    type: "polyline",
                                    hasZ: false,
                                    hasM: true,
                                    paths: evt.geometry.paths,
                                    spatialReference: { wkid: __WEBPACK_IMPORTED_MODULE_5__config_esriConfig__["a" /* default */].wkid }
                                });
                            }
                            else {
                                polygon = new Point({
                                    x: evt.geometry.x,
                                    y: evt.geometry.y,
                                    spatialReference: { wkid: __WEBPACK_IMPORTED_MODULE_5__config_esriConfig__["a" /* default */].wkid }
                                });
                            }
                            mapView.goTo({ target: polygon, zoom: 15 });
                        };
                        this.switchActiveLayer = function (evt) {
                            this.ServiceNameField = __WEBPACK_IMPORTED_MODULE_4__config_config__["a" /* default */][evt].ServiceNameField;
                            this.SelectedURL = __WEBPACK_IMPORTED_MODULE_4__config_config__["a" /* default */][evt].ServiceURL;
                            map.layers.forEach(function (element) {
                                element.visible = false;
                            });
                            var evtNum = parseInt(evt);
                            map.layers.items[evtNum].visible = true;
                            this.features = [];
                        };
                        this.getResults = function (self, evt) {
                            var _this = this;
                            this.loading = true;
                            this.queried = true;
                            this.features = Array();
                            var x = evt.result.feature.geometry.x;
                            var y = evt.result.feature.geometry.y;
                            this._http.get(this.SelectedURL + ("/query?outFields=*&geometry=" + x + "%2C" + y + "&geometryType=esriGeometryPoint&inSR=" + __WEBPACK_IMPORTED_MODULE_5__config_esriConfig__["a" /* default */].wkid + "&returnGeometry=true&returnCurves=false&spatialRel=esriSpatialRelIntersects&distance=" + __WEBPACK_IMPORTED_MODULE_5__config_esriConfig__["a" /* default */].distanceExtentUnit + "&units=" + __WEBPACK_IMPORTED_MODULE_5__config_esriConfig__["a" /* default */].measurement + "&outSR=" + __WEBPACK_IMPORTED_MODULE_5__config_esriConfig__["a" /* default */].wkid + "&f=pjson"))
                                .subscribe(function (data) {
                                if (data !== undefined && Object.keys(data).length !== 0 && data.features !== undefined) {
                                    var _features = data.features.map(function (response) { return response; });
                                    _features.forEach(function (element) {
                                        if (_this.features.findIndex(function (a) { return a.attributes.TRAIL_NAME === element.attributes.TRAIL_NAME; }) <= -1) {
                                            _this.features.push(element);
                                        }
                                    });
                                    var geoService = new GeometryService(__WEBPACK_IMPORTED_MODULE_5__config_esriConfig__["a" /* default */].geoServiceURL);
                                    _this.geoDistance(geoService, DistanceParameters, evt);
                                }
                            });
                        };
                        __WEBPACK_IMPORTED_MODULE_4__config_config__["a" /* default */].forEach(function (layer) {
                            map.layers.add(new FeatureLayer({
                                url: layer.ServiceURL,
                                visible: false
                            }));
                        });
                        map.layers.items[0].visible = true;
                        ext = new Extent({
                            spatialReference: new SpatialReference({ wkid: __WEBPACK_IMPORTED_MODULE_5__config_esriConfig__["a" /* default */].wkid })
                        });
                        mapView = new MapView({
                            // create the map view at the DOM element in this component
                            container: this.mapEl.nativeElement,
                            center: [-78.6382, 35.7796],
                            zoom: 12,
                            map: map,
                            extent: ext
                        });
                        search = new Search({
                            view: mapView,
                            container: this.addressElement.nativeElement
                        });
                        search.on("select-result", (function (evt) { return _this.getResults(_this, evt); }));
                        return [2 /*return*/];
                }
            });
        });
    };
    //Sorts the list by the 'Name' field in the list. 
    GreenwayMapComponent.prototype.sortBy = function (prop) {
        return this.features.sort(function (a, b) { return a[prop] > b[prop] ? 1 : a[prop] === b[prop] ? 0 : -1; });
    };
    //Initialize the map
    GreenwayMapComponent.prototype.ngOnInit = function () {
        this.getGeo();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], GreenwayMapComponent.prototype, "mapEl", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('address'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], GreenwayMapComponent.prototype, "addressElement", void 0);
    GreenwayMapComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'greenway-map',template:/*ion-inline-start:"C:\Users\brian.ferry\Downloads\New folder\greenway-app\src\components\greenway-map\greenway-map.html"*/'<ion-item class="mb-2">\n\n    <ion-label>I\'m looking for...</ion-label>\n\n    <ion-select multiple="false" [(ngModel)]="Selected" (ionChange)="switchActiveLayer($event)">\n\n        <ion-option value="0">Greenways</ion-option>\n\n        <ion-option value="1">Libraries</ion-option>\n\n    </ion-select>\n\n</ion-item>\n\n\n\n<div id="address" #address></div>\n\n\n\n<div *ngIf="loading">\n\n    <ion-spinner name="crescent"></ion-spinner>\n\n    Searching for results...\n\n</div>\n\n\n\n<div *ngIf="!loading && features.length == 0 && queried">\n\n    No Results Found. :(\n\n</div>\n\n\n\n<ion-card class="card-center" *ngFor="let feature of sortBy(\'distance\')" (click)="mapViewZoom(feature)">\n\n    <ion-item>\n\n        <ion-label>{{ feature.attributes[ServiceNameField] }}</ion-label>\n\n    </ion-item>\n\n    <ion-item>\n\n        <ion-label>{{ feature.distance }} miles away</ion-label>\n\n    </ion-item>\n\n</ion-card>\n\n<div id="map" #map></div>'/*ion-inline-end:"C:\Users\brian.ferry\Downloads\New folder\greenway-app\src\components\greenway-map\greenway-map.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Platform */], __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], GreenwayMapComponent);
    return GreenwayMapComponent;
}());

//# sourceMappingURL=greenway-map.js.map

/***/ }),

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const config = [
    {
        ServiceName: "Greenway Trails",
        ServiceURL: "https://maps.wakegov.com/arcgis/rest/services/OpenSpace/Greenways/MapServer/0",
        ServiceNameField: "TRAIL_NAME",
        LayerType: "Map"
    }
    ,{
        ServiceName: "Public Libraries",
        ServiceURL: "https://services1.arcgis.com/a7CWfuGP5ZnLYE7I/arcgis/rest/services/Libraries/FeatureServer/0",
        ServiceNameField: "NAME",
        LayerType: "Feature"
    }
];

/* harmony default export */ __webpack_exports__["a"] = (config);

/***/ }),

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const esriSettings = {
    wkid: 102100,
    measurement: "esriSRUnit_Meter",
    distanceExtentUnit: 10000,
    distanceSearchUnit: "miles",
    geoServiceURL: "http://sampleserver6.arcgisonline.com/arcgis/rest/services/Utilities/Geometry/GeometryServer"
}

/* harmony default export */ __webpack_exports__["a"] = (esriSettings);

/***/ })

},[198]);
//# sourceMappingURL=main.js.map